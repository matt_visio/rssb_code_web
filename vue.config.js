const path = require("path");

module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? '/VisioImpulse/visually-grounded-nlp/rssb_web/'
      : '/'
}