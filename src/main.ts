import  Vue,{ createApp, provide, h } from 'vue'
import App from './App.vue'
import './index.css'

import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client/core'
import { DefaultApolloClient, provideApolloClient } from '@vue/apollo-composable'
import mitt from 'mitt';

const emitter = mitt();

const httpLink = createHttpLink({
  uri: 'https://immortal-koi-12.hasura.app/v1/graphql',
})

const cache = new InMemoryCache()
const apolloClient = new ApolloClient({
  link: httpLink,
  cache,
})

provideApolloClient(apolloClient);

const app = createApp({  
    render: () => h(App),
  }
);
app.config.globalProperties.emitter = emitter;
app.mount('#app')
